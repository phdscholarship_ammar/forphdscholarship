1) Running main.py will automatically call and run the files for the three tasks.
2) ImageSave.py is used to create images from MNIST dataset and save them to directories. (Directories are created if not exist already)
3) SaveImageLocations.py is used to read all the image file locations into Pandas dataframe as save them as CSV file.
4) BatchReadTrain.py is used to read the images from saved CSV file (containing image locations) and train a model in batches.