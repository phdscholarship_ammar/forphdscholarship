# -*- coding: utf-8 -*-
"""
Created on Mon Sep  3 14:51:25 2018

@author: ammarmalik
"""
from ImageSave import ConvertMNIST2Image
from SaveImageLocations import LoadImageDir2Dataframe
from BatchReadTrain import TrainTestOnBatch


def main():
    ConvertMNIST2Image() # Task 1 Convert digits to images and save to directory
    LoadImageDir2Dataframe() # Task 2 Load the image locations to pandas dataframe
    TrainTestOnBatch() # Task 3 Read/Train from dataframe in batches

if __name__ == "__main__":
    main()