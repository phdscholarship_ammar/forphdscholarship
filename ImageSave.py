
# This code converts all the digits in MNIST dataset to images 
# and saves them to a directory
# In[1]:


import os
import warnings
warnings.filterwarnings('ignore')
import scipy.misc
from keras.datasets import mnist

def ConvertMNIST2Image():
    (X_train, y_train), (X_test, y_test) = mnist.load_data()
    
    
    # In[2]:
    
    cwd = os.getcwd()
    train = "Train_Folder"
    test = "Test_Folder"
    if not os.path.exists(train):
        os.makedirs(train)
    if not os.path.exists(test):
        os.makedirs(test)
    
    
    # In[3]:
    
    ## First saving Training Images
    for i in range(10):
        temp = X_train[(y_train == i),:,:]
        x = 0
        for j in range(temp.shape[0]):
            x = x + 1
            temp_img = temp[j]
            file_path = (cwd+"/"+"Train_Folder"+"/"+str(i)+"/")
            directory = os.path.dirname(file_path)
            if not os.path.exists(directory):
                os.makedirs(directory)
            if not os.path.isfile(file_path+str(x)+".png"):    
                scipy.misc.toimage(temp_img, cmin=0.0, cmax=...).save(file_path+str(x)+".png")
    
    
    # In[4]:
    
    ## Now saving Testing Images
    for i in range(10):
        temp = X_test[(y_test == i),:,:]
        x = 0
        for j in range(temp.shape[0]):
            x = x + 1
            temp_img = temp[j]
            file_path = (cwd+"/"+"Test_Folder"+"/"+str(i)+"/")
            directory = os.path.dirname(file_path)
            if not os.path.exists(directory):
                os.makedirs(directory)
            if not os.path.isfile(file_path+str(x)+".png"):
                scipy.misc.toimage(temp_img, cmin=0.0, cmax=...).save(file_path+str(x)+".png")

