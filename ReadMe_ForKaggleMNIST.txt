1) I have uploaded the 2 versions for the Kaggle MNIST submission.
2) Version 1 contains my initial submission which used simple CNN model.
3) Version 2 contains latest submission using "Xception". (using MNIST + my own created images for training).
4) The score of Version 1 on Kaggle Leaderboard is 0.98800.
5) The score of Version 2 on Kaggle Leaderboard is 0.90071.
6) The accuracy is not improving using Xception probably because
	-> The image dimensions have to be increased to 71 x 71 for Xception
	-> The Xception model is overfitting the training data perhaps
	-> Further fine tuning may increase the results
 