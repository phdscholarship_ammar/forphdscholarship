
# coding: utf-8

# In[1]:
import pandas as pd
import numpy as np
from keras.preprocessing.image import load_img 
from keras.preprocessing.image import img_to_array
from keras.models import Sequential, Model
from keras.layers import Conv2D, MaxPooling2D, GlobalAveragePooling2D
from keras.layers import Dropout, Flatten, Dense
from keras.utils import np_utils
from keras.applications import Xception
from sklearn.metrics import confusion_matrix

img_rows = 71
img_cols = 71
num_channels = 3
num_classes = 10
batch_size = 32

###############################################################################################################
def generator_from_df(df, batch_size, target_size):
    
    nbatches, n_skipped_per_epoch = divmod(df.shape[0], batch_size)
    # In each batch integer number of examples will be used
    # therefore some examples will be skipped in each batch.
    # But eventually the model will see all the examples because of the shuffling before each epoch.
    count = 1
    epoch = 0
    num_classes = 10
    
    while 1:
        df = df.sample(frac=1) # Shuffle the dataframe
        mini_batches_completed = 0
        epoch += 1
        i, j = 0, batch_size
        for _ in range(nbatches):
            sub = df.iloc[i:j]
            X = np.array([ ((img_to_array(load_img(f, target_size=(target_size),grayscale=False))))
                for f in sub.imgpath])
            X = X/255 # Normalizing the images
            Y = sub.target.values
            Y = np_utils.to_categorical(Y,num_classes) # Converting the output classes to categorical
            mini_batches_completed += 1
            yield X,Y
        i = j
        j += batch_size
        count += 1
################################################################################################################
        
trainfiles = pd.read_csv('Train_Files.csv')
testfiles = pd.read_csv('Test_Files.csv')

base_model = Xception(weights='imagenet',
                      include_top=False,
                      input_shape=(img_rows, img_cols, num_channels))
#base_model.summary()

x = base_model.output
x = GlobalAveragePooling2D()(x)
predictions = Dense(num_classes, activation='softmax')(x)

model = Model(base_model.input, predictions)
model.compile(optimizer='nadam',loss='categorical_crossentropy',metrics=['accuracy'])

    
df_train = trainfiles
df_test = testfiles
train_generator = generator_from_df(df_train, batch_size, target_size = [img_rows,img_cols,num_classes])
test_generator = generator_from_df(df_test, batch_size, target_size = [img_rows,img_cols,num_classes])
nbatches_train, mod = divmod(df_train.shape[0], batch_size)
nbatches_test, mod = divmod(df_test.shape[0],  batch_size) # batch_size =



# In[5]:

nworkers = 10
model.fit_generator(
    train_generator,
    steps_per_epoch=nbatches_train,
   # Uncomment the following 2 lines if testing is also required using batches     
    validation_data = test_generator,
    validation_steps = nbatches_test,
    epochs=10,
    verbose=1,
    workers = nworkers)

