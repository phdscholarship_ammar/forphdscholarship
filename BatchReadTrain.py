
# coding: utf-8

# In[1]:
import pandas as pd
import numpy as np
from keras.preprocessing.image import load_img 
from keras.preprocessing.image import img_to_array
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Dropout, Flatten, Dense
from keras.utils import np_utils


def generator_from_df(df, batch_size, target_size):
    
    nbatches, n_skipped_per_epoch = divmod(df.shape[0], batch_size)
    # In each batch integer number of examples will be used
    # therefore some examples will be skipped in each batch.
    # But eventually the model will see all the examples because of the shuffling before each epoch.
    count = 1
    epoch = 0
    num_classes = 10
    
    while 1:
        df = df.sample(frac=1) # Shuffle the dataframe
        mini_batches_completed = 0
        epoch += 1
        i, j = 0, batch_size
        for _ in range(nbatches):
            sub = df.iloc[i:j]
            X = np.array([ ((img_to_array(load_img(f, target_size=(target_size),grayscale=True))))
                for f in sub.imgpath])
            X = X/255 # Normalizing the images
            Y = sub.target.values
            Y = np_utils.to_categorical(Y,num_classes) # Converting the output classes to categorical
            mini_batches_completed += 1
            yield X,Y
        i = j
        j += batch_size
        count += 1

def TrainTestOnBatch():
    trainfiles = pd.read_csv('TrainFileLocations.csv')
    testfiles = pd.read_csv('TestFileLocations.csv')
    
    # In[2]:
    model = Sequential()
    model.add(Conv2D(32, (5, 5), input_shape=(28, 28, 1), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.2))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dense(10, activation='softmax'))
    # Compile model
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.summary()    
    # In[4]:
        
    df_train = trainfiles
    df_test = testfiles
    batch_size = 100
    img_rows = 28
    img_cols = 28
    train_generator = generator_from_df(df_train, batch_size, target_size = [img_rows,img_cols])
    test_generator = generator_from_df(df_test, batch_size, target_size = [img_rows,img_cols])
    nbatches_train, mod = divmod(df_train.shape[0], batch_size)
    nbatches_test, mod = divmod(df_test.shape[0], batch_size)

    
    
    # In[5]:
    
    nworkers = 10
    model.fit_generator(
        train_generator,
        steps_per_epoch=nbatches_train,
   # Uncomment the following 2 lines if testing is also required using batches     
   #     validation_data = test_generator,
   #     validation_steps = nbatches_test,
        epochs=10,
        verbose=1,
        workers = nworkers)

