import os
import pandas as pd

# This code creates a Pandas Dataframe 
# containing locations of all images

def LoadImageDir2Dataframe():
    # For Training Data
    filenames = pd.DataFrame([])
    current = os.getcwd()
    for i in range(10):
        folder = current + ("\\")+("Train_Folder")+("\\")+str(i)+("\\")
        filelist = os.listdir(folder)
        for j in range(len(filelist)):
            temp = pd.DataFrame([os.path.join(folder,filelist[j])])
            temp['target'] = i
            filenames = pd.concat([filenames,temp],ignore_index=True)
    
    filenames = filenames.rename(columns = {0:'imgpath'})
    filenames.to_csv('TrainFileLocations.csv',index=None)
    
    # For Training Data
    filenames = pd.DataFrame([])
    current = os.getcwd()
    for i in range(10):
        folder = current + ("\\")+("Test_Folder")+("\\")+str(i)+("\\")
        filelist = os.listdir(folder)
        for j in range(len(filelist)):
            temp = pd.DataFrame([os.path.join(folder,filelist[j])])
            temp['target'] = i
            filenames = pd.concat([filenames,temp],ignore_index=True)
    
    filenames = filenames.rename(columns = {0:'imgpath'})
    filenames.to_csv('TestFileLocations.csv',index=None)