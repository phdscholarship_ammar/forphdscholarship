# -*- coding: utf-8 -*-
"""
Created on Thu Sep  6 14:03:54 2018

@author: ammarmalik
"""

import os
import warnings
warnings.filterwarnings('ignore')
import pandas as pd
import numpy as np
import scipy.misc
from keras.preprocessing.image import load_img 
from keras.preprocessing.image import img_to_array
from keras.models import Sequential, Model
from keras.layers import Conv2D, MaxPooling2D, GlobalAveragePooling2D
from keras.layers import Dropout, Flatten, Dense
from keras.utils import np_utils
from keras.applications import Xception
from PIL import Image, ImageDraw, ImageFont
from random import randint

#from sklearn.metrics import confusion_matrix
#from sklearn.model_selection import train_test_split
#from keras import optimizers
#from keras.datasets import mnist
#from skimage.transform import resize

def CreateFontDigits():    
    font_names = ['AGENCYB','AGENCYR','arial','arialbd','arialbi','ariali','calibri','calibrib','calibril','times']
    fonts_folder = ('C://Windows//Fonts//')
    num_fonts = 10
    num_classes = 10 # no. of digits
    total_imgs = 100000
    digit_per_font = int((total_imgs/num_classes)/num_fonts)
    img_rows = 71
    img_cols = 71   
    folder = "Train_Folder"
    current = os.getcwd()
    if not os.path.exists(folder):
        os.makedirs(folder)
    for i in range(num_classes):
            file_path = (current+"/"+"Train_Folder"+"/"+str(i)+"/")
            directory = os.path.dirname(file_path)
            if not os.path.exists(directory):
                os.makedirs(directory)
            x = 0
            for j in range(digit_per_font):
                for k in range(num_fonts):
                    x = x + 1
                    img = Image.new('RGB', (img_rows, img_cols),color = (0, 0, 0) )
                    fnt = ImageFont.truetype((fonts_folder+font_names[k]+".ttf"),randint(35, 40)) # randomly set the size so that each digit has different size
                    d = ImageDraw.Draw(img)
                    d.text((20,10), str(i) , font=fnt, fill=(255, 255, 255))
                    img.save((file_path+str(x).zfill(5)+".png"))
                    

def ConvertMNIST2Image():
    train_csv = pd.read_csv("train.csv")
    test_csv = pd.read_csv("test.csv")
    
    y_train = train_csv["label"]
    X_train = train_csv.drop("label", axis=1)
    X_test = test_csv
    cwd = os.getcwd()
    train = "Train_Folder"
    test = "Test_Folder"
    if not os.path.exists(train):
        os.makedirs(train)
    if not os.path.exists(test):
        os.makedirs(test)
    
    # Images will have to be padded with zeros so that the image size is 71 x 71
    # which is the requirement for Xception classifier
    num_classes = 10
    mnist_height = 28
    mnist_width = 28
    xception_height = 71
    xception_width = 71
    padd_row = int((xception_width - mnist_width)/2)+1
    padd_col = int((xception_height - mnist_height)/2)
    for i in range(num_classes):
        temp = X_train.loc[(y_train == i),:]
        x = 0
        for j in range(temp.shape[0]):
            x = x + 1
            temp_img = temp.iloc[j].reshape(mnist_height,mnist_width)
            temp_img = np.pad(temp_img,(padd_row,padd_col), 'constant', constant_values=(0))
            file_path = (cwd+"/"+"Train_Folder"+"/"+str(i)+"/")
            directory = os.path.dirname(file_path)
            if not os.path.exists(directory):
                os.makedirs(directory)
 #           if not os.path.isfile(file_path+str(x+10000).zfill(5)+".png"):    
            scipy.misc.toimage(temp_img, cmin=0.0, cmax=255.0).save(file_path+str(x+10000).zfill(5)+".png")
    
    x = 0
    for j in range(X_test.shape[0]):
        x = x + 1
        temp_img = X_test.iloc[j].reshape(mnist_height,mnist_width)
        temp_img = np.pad(temp_img,(padd_row,padd_col), 'constant', constant_values=(0))
        file_path = (cwd+"/"+"Test_Folder"+"/")
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
 #       if not os.path.isfile(file_path+str(x).zfill(5)+".png"):
        scipy.misc.toimage(temp_img, cmin=0.0, cmax=255.0).save(file_path+str(x).zfill(5)+".png")


def LoadImageDir2Dataframe():
    # For Training Data
    filenames = pd.DataFrame([])
    current = os.getcwd()
    num_classes = 10
    for i in range(num_classes):
        folder = current + ("\\")+("Train_Folder")+("\\")+str(i)+("\\")
        filelist = os.listdir(folder)
        for j in range(len(filelist)):
            temp = pd.DataFrame([os.path.join(folder,filelist[j])])
            temp['target'] = i
            filenames = pd.concat([filenames,temp],ignore_index=True)
    
    filenames = filenames.rename(columns = {0:'imgpath'})
    filenames.to_csv('TrainFileLocations.csv',index=None)
    
    # For Testing Data
    filenames = pd.DataFrame([])
    current = os.getcwd()
    folder = current + ("\\")+("Test_Folder")+("\\")#+str(i)+("\\")
    filelist = os.listdir(folder)
    for j in range(len(filelist)):
        temp = pd.DataFrame([os.path.join(folder,filelist[j])])
    #        temp['target'] = i
        filenames = pd.concat([filenames,temp],ignore_index=True)
    
    filenames = filenames.rename(columns = {0:'imgpath'})
    filenames.to_csv('TestFileLocations.csv',index=None)
    

def generator_from_df(df, batch_size, target_size): # for training 
    
    nbatches, n_skipped_per_epoch = divmod(df.shape[0], batch_size)
    # In each batch integer number of examples will be used
    # therefore some examples will be skipped in each batch.
    # But eventually the model will see all the examples because of the shuffling before each epoch.
    count = 1
    epoch = 0
    num_classes = 10
    
    while 1:
        df = df.sample(frac=1) # Shuffle the dataframe
        mini_batches_completed = 0
        epoch += 1
        i, j = 0, batch_size
        for _ in range(nbatches):
            sub = df.iloc[i:j]
            X = np.array([ ((img_to_array(load_img(f, target_size=(target_size),grayscale=False))))
                for f in sub.imgpath])
            X = X/255 # Normalizing the images
            Y = sub.target.values
            Y = np_utils.to_categorical(Y,num_classes) # Converting the output classes to categorical
            mini_batches_completed += 1
            yield X,Y
        i = j
        j += batch_size
        count += 1
        

def Xception_Model_Classification(Classification_Model):
    img_rows = 71
    img_cols = 71
    num_channels = 3
    num_classes = 10
    batch_size = 256
    trainfiles = pd.read_csv('TrainFileLocations.csv')
    testfiles = pd.read_csv('TestFileLocations.csv')
    
    if Classification_Model == 'Xception':
        base_model = Xception(weights='imagenet',
                              include_top=False,
                              input_shape=(img_rows, img_cols, num_channels))
        #base_model.summary()
    
            
        x = base_model.output
        x = GlobalAveragePooling2D()(x)
        x = (Dense(128, activation='relu'))(x)
        predictions = Dense(num_classes, activation='softmax')(x)
        
        model = Model(base_model.input, predictions)
    else: #Simple CNN Model
        model = Sequential()
        model.add(Conv2D(32, (5, 5), input_shape=(71, 71, 3), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.2))
        model.add(Flatten())
        model.add(Dense(128, activation='relu'))
        model.add(Dense(10, activation='softmax'))
        
        
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    
    df_train = trainfiles
    train_generator = generator_from_df(df_train, batch_size, target_size = [img_rows,img_cols,num_classes]) # Train_Files
    test_imgs =(np.array([ ((img_to_array(load_img(f, target_size=(71,71,3),grayscale=False)))) for f in testfiles.imgpath]))/255 # Test_Files
    nbatches_train, mod = divmod(df_train.shape[0], batch_size)
    nworkers = 3
    model.fit_generator(
        train_generator,
        steps_per_epoch=nbatches_train,
        epochs=10,
        verbose=1,
        workers = nworkers)
    #    predictions = model.predict_generator(test_generator,nbatches_test,verbose=1)
    predictions = model.predict(test_imgs,verbose=1)
    DF = pd.DataFrame({'Label':np.argmax(predictions, axis=1)})
    DF['ImageId']=DF.index+1
    columnsTitles=["ImageId","Label"]
    DF=DF.reindex(columns=columnsTitles)
    DF.to_csv('MNISTPredictions.csv',index=None)
    
def main():
# Uncomment the following 
    
#    CreateFontDigits() # Create 10000 digits with different fonts
#    ConvertMNIST2Image() # Convert digits to images and save to directory
#    LoadImageDir2Dataframe() # Load the image locations to pandas dataframe
   Xception_Model_Classification(Classification_Model='Xception') # Read/Train from dataframe in batches

if __name__ == "__main__":
    main()    