
# coding: utf-8
"@author: ammarmalik"
# In[1]:
import os
from random import randint
import pandas as pd
from sklearn.model_selection import train_test_split
from PIL import Image, ImageDraw, ImageFont

# I decided to choose 10 fonts randomly 
font_names = ['AGENCYB','AGENCYR','arial','arialbd','arialbi','ariali','calibri','calibrib','calibril','times']
fonts_folder = ('C://Windows//Fonts//')
num_fonts = 10
num_classes = 10 # no. of digits
total_imgs = 100000
digit_per_font = int((total_imgs/num_classes)/num_fonts)
# I chose 71 x 71 images because later I will use the weights from
# pretrained "imagenet", for which the image should be atleast 71 x 71
img_rows = 71
img_cols = 71


# In[4]:
folder = "100000Digits"
current = os.getcwd()
if not os.path.exists(folder):
    os.makedirs(folder)
# In[5]:
for i in range(num_classes):
        file_path = (current+"/"+"100000Digits"+"/"+str(i)+"/")
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
        x = 0
        for j in range(digit_per_font):
            for k in range(num_fonts):
                x = x + 1
                img = Image.new('RGB', (img_rows, img_cols),color = (0, 0, 0) )
                fnt = ImageFont.truetype((fonts_folder+font_names[k]+".ttf"),randint(45, 50)) # randomly set the size so that each digit has different size
                d = ImageDraw.Draw(img)
                d.text((20,10), str(i) , font=fnt, fill=(255, 255, 255))
                img.save((file_path+str(x)+".png"))
                


# In[6]:
filenames = pd.DataFrame([])
for i in range(num_classes):
    folder = current + ("\\")+("100000Digits")+("\\")+str(i)+("\\")
    filelist = os.listdir(folder)
    for j in range(len(filelist)):
        temp = pd.DataFrame([os.path.join(folder,filelist[j])])
        temp['target'] = i
        filenames = pd.concat([filenames,temp],ignore_index=True)

filenames = filenames.rename(columns = {0:'imgpath'})


# In[7]:
X = filenames.imgpath
y = filenames.target

# I have decided to split the images to training and testing sets here
# so that the next code is only used for classification using Xception

X_train, X_test, y_train, y_test = train_test_split(X, y,test_size=0.2)


# In[8]:
Train_Files = pd.DataFrame([])
Train_Files['imgpath'] =  X_train
Train_Files['target'] =  y_train
Train_Files.to_csv('Train_Files.csv',index=None)


# In[9]:
Test_Files = pd.DataFrame([])
Test_Files['imgpath'] =  X_test
Test_Files['target'] =  y_test
Test_Files.to_csv('Test_Files.csv',index=None)

